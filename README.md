
Mi primer proyecto con NodeJS y express (Folder red-bicicletas):

El proyecto debe cumplir con los siguientes puntos:

1.- un archivo README (sólo texto) en el repositorio de Bitbucket. (este mismo archivo).
2.- el mensaje de bienvenida a Express.
3.- el proyecto vinculado con el repositorio de Bitbucket creado previamente. https://edgaalejandrotorres@bitbucket.org/edgaalejandrotorres/proyecto1.git
4.- el servidor que se ve correctamente, comparándolo con la versión original.
5- un mapa centrado en una ciudad.
6- marcadores indicando una  ubicación.
7.- un script npm en el archivo package.json, debajo del “start”, que se llama “devstart” y que levanta el servidor utilizando npdemon.
8.- un par de bicicletas en la colección que conoce la bicicleta en memoria, con las ubicaciones cercanas al centro del mapa agregado.
9.- una colección del modelo de bicicleta.
10.- los endpoints de la API funcionando con Postman.

https://edgaalejandrotorres@bitbucket.org/edgaalejandrotorres/proyecto1.git

------------------
utilizando MongoDB, Mongoose y Jasmine:

En tu proyecto deberá verse

1.     una carpeta con nombre “models” dentro de la carpeta spec.
2.     los tests aprobados
3.     tests de cada operación de la API de bicicleta.
4.     un archivo bicicleta_api_test.spec.js
5.     todos los tests aprobados al ejecutar el comando npm test.
6.     las colecciones actuales Desde Mongo Compass, conectado a tu base local de Mongo.
7.     una base local mongo llamada “red_bicicletas” conectada usando mongoose.
8.     el modelo funcional utilizando Postman.
9.     documentos generados en la base local con Mongo Compass.
10. tests del modelo Bicicleta que usan persistencia.

inicializar con:  npm install
npm run devstart