var mongoose = require('mongoose');
var Bicicleta = require("../../models/bicicleta");

var request = require('request');
var server = require('../../bin/www');
var base_url = 'http://localhost:3000/api/bicicletas';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 100000;

describe('Bicicleta API', function() {
	beforeAll(function(done){
		var mongoDB = 'mongodb://localhost/test';

		// mongoose.createConnection es usado por que el server ya tiene una conexion creada para red_bicicletas
		const conn = mongoose.createConnection(mongoDB, {useNewUrlParser: true});
		const dbtest = conn;
		dbtest.on('error', console.error.bind(console, 'connection error:'));
		dbtest.once('open', function(){
			console.log('We are connected to the database');
		});
		done();
	});

	afterEach(function(done){
		Bicicleta.deleteMany({}, function(err, success){
			if (err) console.log(err);
			done();
		});
	});

	afterAll(function(done){
		mongoose.disconnect();
		done();
	});

	describe('GET BICICLETAS /', () => {
		it('Status 200', (done) => {
			Bicicleta.allBicis(function(err, bicis){
				expect(bicis.length).toBe(0);
			});
			
			request.get(base_url, function(error, response, body){
				expect(response.statusCode).toBe(200);
				done();
			});			
		});
	});

	describe('POST BICICLETAS /create', () => {
		it('Status 200', (done) => {
			Bicicleta.allBicis(function(err, bicis){
				expect(bicis.length).toBe(0);
			});
			
			var headers = {'content-type': 'application/json'}
			var aBici = '{ "code": 10, "color": "rojo", "modelo": "urbana", "lat": -34.6012424, "lng": -58.3861497 }';
			request.post({
				headers: headers,
				url: base_url + '/create',
				body: aBici
			}, function(error, response, body){
				expect(response.statusCode).toBe(200);
				Bicicleta.allBicis(function(err, bicis){
					expect(bicis.length).toEqual(1);
					expect(bicis[0].color).toEqual("rojo");
					done();
				});
				Bicicleta.findByCode(10, function(err, aBiciFound){
					expect(aBiciFound.color).toBe("rojo");
				});
				done();
			});
		});
	});

	describe('PUT BICICLETAS /update', () => {
		it('Status 200', (done) => {
			Bicicleta.allBicis(function(err, bicis){
				expect(bicis.length).toBe(0);
			});

			var bici = Bicicleta.createInstance(11, 'blanca', 'urbana');
			bici.ubicacion = [-34.6012424, -58.3861497];
			Bicicleta.add(bici);
			
			var headers = {'content-type': 'application/json'}
			var aBici = '{ "code": 11, "color": "rojo", "modelo": "montaña", "lat": -34.6012424, "lng": -58.3861497 }';
			request.put({
				headers: headers,
				url: base_url + '/update',
				body: aBici
			}, function(error, response, body){
				expect(response.statusCode).toBe(200);

				Bicicleta.findByCode(11, function(err, aBiciFound){
					console.log('updated bici=' + aBiciFound);
					expect(aBiciFound.color).toBe("rojo");
				});
				done();
			});
		});
	});

	describe('DELETE BICICLETAS /delete', () => {
		it('Status 200', (done) => {
			Bicicleta.allBicis(function(err, bicis){
				expect(bicis.length).toBe(0);
			});
			
			var bici = Bicicleta.createInstance(10, 'negro', 'urbana');
			bici.ubicacion = [-34.6012424, -58.3861497];
			Bicicleta.add(bici);
			
			var headers = {'content-type': 'application/json'}
			var aBiciToRemove = '{ "code": 10 }';
						
			request.post({
				headers: headers,
				url: base_url + '/delete',
				body: aBiciToRemove
			}, function(error, response, body){
				expect(response.statusCode).toBe(200);
				Bicicleta.allBicis(function(err, bicis){
					console.log('bicis: ' + bicis)
					expect(bicis.length).toBe(0);
				});
				done();
			});
		});
	});	
	
});
