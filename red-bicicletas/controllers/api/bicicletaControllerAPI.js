var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req, res){
    Bicicleta.allBicis(function(err, bicis){
        res.status(200).json({bicicletas: bicis});
    });
}

exports.bicicleta_create = function(req, res){
    var bici = Bicicleta.createInstance(req.body.code, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.lng];
    
    Bicicleta.add(bici);

    res.status(200).json({
        bicicleta: bici
    });
}

exports.bicicleta_delete = function(req, res){
    Bicicleta.removeByCode(req.body.code, function(){
        res.status(200).send();
    });
}

exports.bicicleta_update = function(req, res){
    Bicicleta.findByCode(req.body.code, function(err, biciFound){
        if (biciFound){
            Bicicleta.updateByCode(req.body.code, req.body.color, req.body.modelo,
                req.body.lat, req.body.lng, function(err, biciUpdated){
                if (biciUpdated){  
                    res.status(200).json({
                        bicicleta: biciUpdated
                    });
                } else{
                    console.log('Error: ' + err);
                    res.status(400).send();
                }
            });
        } else {
            console.log('Error: ' + err);
            res.status(400).send();
        }
    });
}
