// Controller
var Bicicleta = require('../models/bicicleta');

exports.bicicleta_list = function(req, res){
    Bicicleta.allBicis(function(err, bicis){
        res.render('bicicletas/index', { bicis : bicis } );
    });
}

exports.bicicleta_create_get = function(req, res){
    res.render('bicicletas/create');
}

exports.bicicleta_create_post = function(req, res){
    var bici = Bicicleta.createInstance(req.body.code, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.lng];
    Bicicleta.add(bici);
    res.redirect('/bicicletas');
}

exports.bicicleta_delete_post = function(req, res){
    console.log('Removing: ' + req.body.code);
    Bicicleta.removeByCode(req.body.code, function(err, bici){
        res.redirect('/bicicletas');
    });
}

exports.bicicleta_update_get = function(req, res){
    Bicicleta.findByCode(req.params.code, function(err, bici){
        res.render('bicicletas/update', {bici});
    });
}

exports.bicicleta_update_post = function(req, res){
    Bicicleta.findByCode(req.body.code, function(err, biciFound){
        if (biciFound){
            Bicicleta.updateByCode(req.body.code, req.body.color, req.body.modelo,
                req.body.lat, req.body.lng, function(err, biciUpdated){
                if (biciUpdated){
                    console.log('bici updated: ' + biciUpdated);
                    res.redirect('/bicicletas');
                }else{
                    console.log('Error: ' + err);
                    res.redirect('/bicicletas');
                }
            });
        } else {
            console.log('Error: ' + err);
            res.redirect('/bicicletas');
        }
    });
    
}
