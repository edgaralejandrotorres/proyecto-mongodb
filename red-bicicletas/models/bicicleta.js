var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
	code: Number, 
	color: String,
	modelo: String,
	ubicacion: {
		type: [Number], index: { type: '2dsphere', sparse: true}
	}
});

bicicletaSchema.methods.toString = function(){
    return 'code: '+ this.code + ' color:' + this.color + ' modelo:' + this.modelo;
}

bicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion){
	return new this({
		code: code, 
		color: color,
		modelo: modelo,
		ubicacion: ubicacion
	});
};

bicicletaSchema.statics.allBicis = function(callback){
	return this.find({}, callback);
}

bicicletaSchema.statics.add = function(aBici, callback){
	this.create(aBici, callback);
}

bicicletaSchema.statics.findByCode = function(aCode, callback){
	return this.findOne({code: aCode}, callback);
}

bicicletaSchema.statics.removeByCode = function(aCode, callback){
	return this.deleteOne({code: aCode}, callback);
}

bicicletaSchema.statics.updateByCode = function(aCode, color, modelo, lat, lng, callback){
    return this.updateOne({code: aCode}, 
        { $set: {"color": color, "modelo": modelo, "ubicacion": [lat, lng] } }, callback);
}

module.exports = mongoose.model('Bicicleta', bicicletaSchema);
